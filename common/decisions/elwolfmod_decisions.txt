operations = {
    
    assume_control_malaysia = {
        icon = generic_operation

        allowed = {
            tag = ENG
        }

        available = {
        }

        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        visible = {
            tag = ENG
        }

        complete_effect = {
            ENG = {
                annex_country = {
                target = MAL
                    transfer_troops = no
                }
            }
            every_state = {
                limit = {
                    is_core_of = MAL
                    is_controlled_by = ENG
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_siam = {
        icon = generic_operation

        allowed = {
            tag = JAP
        }

        available = {
            SIA = { is_ai = yes }
        }

        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        visible = {
            tag = JAP
        }

        complete_effect = {
            JAP = {
                annex_country = {
                    target = SIA
                    transfer_troops = no
                }
            }
            every_state = {
                limit = {
                    is_core_of = SIA
                    is_controlled_by = JAP
                }
                set_compliance = 100
		    }
        }
    }

    assume_control_manchukuo = {
        icon = generic_operation

        allowed = {
            tag = JAP
        }

        available = {
            MAN = { is_ai = yes }
        }

        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        visible = {
            tag = JAP
        }

        complete_effect = {
            JAP = {
                annex_country = {
                    target = MAN
                    transfer_troops = yes
                }
                add_research_slot = 1
            }
            every_state = {
                limit = {
                    is_core_of = MAN
                    is_controlled_by = JAP
                }
                set_compliance = 100
		    }
        }
    }

    assume_control_philipines = {
        icon = generic_operation

        allowed = {
            tag = USA
        }

        available = {
            PHI = { is_ai = yes }
        }

        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        visible = {
            tag = USA
        }

        complete_effect = {
            USA = {
                annex_country = {
                    target = PHI
                    transfer_troops = no
                }
            }
            every_state = {
                limit = {
                    is_core_of = PHI
                    is_controlled_by = USA
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_new_zealand = {
        icon = generic_operation

        allowed = {
            tag = AST
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            NZL = { is_ai = yes }
        }

		visible = {}

        complete_effect = {
            AST = {
                annex_country = {
                    target = NZL
                    transfer_troops = yes
                }
            }
            every_state = {
                limit = {
                    is_core_of = NZL
                    is_controlled_by = AST
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_canada = {
        icon = generic_operation

        allowed = {
            tag = ENG
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            CAN = { is_ai = yes }
        }

		visible = {}

        complete_effect = {
            ENG = {
                annex_country = {
                    target = CAN
                    transfer_troops = yes
                }
            }
            every_state = {
                limit = {
                    is_core_of = CAN
                    is_controlled_by = ENG
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_south_africa = {
        icon = generic_operation

        allowed = {
            tag = ENG
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            SAF = { is_ai = yes }
        }

		visible = {}

        complete_effect = {
            ENG = {
                annex_country = {
                    target = SAF
                    transfer_troops = yes
                }
            }
            every_state = {
                limit = {
                    is_core_of = SAF
                    is_controlled_by = ENG
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_british_raj = {
        icon = generic_operation

        allowed = {
            tag = ENG
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            RAJ = { is_ai = yes }
        }

		visible = {}

        complete_effect = {
            ENG = {
                annex_country = {
                    target = RAJ
                    transfer_troops = yes
                }
            }
            every_state = {
                limit = {
                    is_core_of = RAJ
                    is_controlled_by = ENG
                }
                set_compliance = 50
		    }
        }
    }
    assume_control_australia = {
        icon = generic_operation

        allowed = {
            tag = ENG
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            AST = { is_ai = yes }
        }

		visible = {}

        complete_effect = {
            ENG = {
                annex_country = {
                    target = AST
                    transfer_troops = yes
                }
            }
            every_state = {
                limit = {
                    is_core_of = AST
                    is_controlled_by = ENG
                }
                set_compliance = 50
		    }
        }
    }
    launch_dday = {
        icon = generic_operation

        allowed = {
            OR = {
                tag = ENG
                tag = USA
            }
        }
        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        available = {
            AND = {
                USA = { has_war_with = GER }
                ENG = { has_war_with = GER }
                16 = { is_controlled_by = GER }
            }
        }

		visible = {
            OR = {
                tag = ENG
                tag = USA
            }
        }

        days_remove = 180
		targeted_modifier = {
			tag = GER
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
        targeted_modifier = {
			tag = ITA
			attack_bonus_against = 0.1
			defense_bonus_against = 0.1
		}
    }
    GER_vichy = {

        fire_only_once = yes
        icon = ger_reichskommissariats

        visible = {
            tag = GER
            has_war_with = FRA
            #date < 1941.1.1
        }

        available = {
            date < 1941.1.1
            FRA = { has_capitulated = yes }
            16 = { is_controlled_by = GER }
        }

        complete_effect = {
            hidden_effect = { news_event = { id = news.34 hours = 6 } elwolfmod_vichy_france = yes  }
        }

    }
}

political_actions = {
    join_allies = {
        allowed = {
            OR = {
                tag = USA
                tag = IRE
                tag = BRA
                tag = MEX
            }
        }
        available = {
            OR = {
                FRA = {
                    has_capitulated = yes
                }
                NOT = {
                    FRA = {
                        controls_state = 16
                    }
                }
            }
            USA = {
                has_war_with = GER
            }
            ROOT = { is_ai = no }
        }

        fire_only_once = yes

        cost = 0

        ai_will_do = {
            factor = 1
        }

        visible = {
            OR = {
                tag = USA
                tag = IRE
                tag = BRA
                tag = MEX
            }
        }

        complete_effect = {
            ENG = {
                add_to_faction = ROOT
            }
        }
    }
}

#####Soviet Union#####

SOV_spain = {
	SOV_assist_republican_spain = {
		icon = spa_garrison_control_none

		fire_only_once = yes

		days_remove = 70

		cost = 0

		available = {
			has_global_flag = spanish_civil_war
		}

		modifier = {}

		remove_effect = {
			SOV = {
				army_experience = 300
				add_political_power = -100
				add_timed_idea = { idea = SPR_seized_spanish_gold_reserves days = 365 }
			}
		}
	}
}

SOV_socialism_in_one_country = {
	
	SOV_claims_on_poland = {
		icon = eng_trade_unions_support

		fire_only_once = yes

		days_remove = 70

		cost = 0

		available = {
			date > 1941.5.1
			has_idea = SOV_molotov_ribbentrop_pact
			NOT = { OR = { has_war_with = POL has_war = yes } }
		}

		modifier = {}

		remove_effect = {
			SOV = {
				transfer_state = 784
				transfer_state = 95
				transfer_state = 96
				transfer_state = 94
				transfer_state = 93
				transfer_state = 91
				transfer_state = 89
			}
		}
	}

	SOV_claims_on_baltic = {
		icon = eng_trade_unions_support

		fire_only_once = yes

		days_remove = 75 	

		cost = 50 	

		available = {
			has_idea = SOV_molotov_ribbentrop_pact
			NOT = { has_war_with = EST }
			NOT = { has_war_with = LIT }
			NOT = { has_war_with = LAT }
		}

		modifier = {}

		remove_effect = {

		    SOV = {
				annex_country = { target = EST transfer_troops = no }
				annex_country = { target = LAT transfer_troops = no }
				annex_country = { target = LIT transfer_troops = no }
                add_equipment_to_stockpile = { type = infantry_equipment_1 amount = 107733 }
				add_equipment_to_stockpile = { type = infantry_equipment_2 amount = 13352 }
			}
			news_event = { id = news.69 }
		}
		ai_will_do = {
			factor = 100
		}
	}

	SOV_claim_bessarabia = {
		icon = eng_trade_unions_support

		fire_only_once = yes

		days_remove = 25 	

		cost = 50 	

		available = {
			has_idea = SOV_molotov_ribbentrop_pact
			NOT = { has_war_with = ROM }
		}

		modifier = {
		}

		remove_effect = {
			SOV = { 
				transfer_state = 78
				transfer_state = 80
				transfer_state = 766
			}
			hidden_effect = {
				SOV = {
					news_event = { days = 1 id = news.10 }
				}
				news_event = { id = news.199 }
			}
		}
		ai_will_do = {
			factor = 100		
		}
	}
	
}

FRA_intervention_in_overseas_territories = {

	FRA_take_north_africa = {
		visible = {
			has_completed_focus = FRA_intervention_in_north_africa
		}
		fire_only_once = yes
		cost = 50
		days_remove = 70
		remove_effect = {
			transfer_state = 513
			transfer_state = 461
			transfer_state = 462
			transfer_state = 514
			transfer_state = 459
			transfer_state = 460
			transfer_state = 665
			transfer_state = 458
			VIC = { country_event = LaR_france_vichy_notification_events.11 }
			GER = { country_event = LaR_france_vichy_notification_events.11 }
		}
	}

	FRA_take_west_africa = {
		visible = {
			has_completed_focus = FRA_intervention_in_west_africa
		}
		fire_only_once = yes
		cost = 50
		days_remove = 70
		remove_effect = {
			transfer_state = 515
            515 = { add_extra_state_shared_building_slots = 2 add_building_construction = { type = arms_factory level = 2 instant_build = yes } }
			transfer_state = 782
            782 = { add_extra_state_shared_building_slots = 1 add_building_construction = { type = arms_factory level = 1 instant_build = yes } }
			transfer_state = 786
            786 = { add_extra_state_shared_building_slots = 1 add_building_construction = { type = arms_factory level = 1 instant_build = yes } }
			transfer_state = 557
            557 = { add_extra_state_shared_building_slots = 1 add_building_construction = { type = arms_factory level = 1 instant_build = yes } }
			transfer_state = 272
			transfer_state = 780
			transfer_state = 556
			transfer_state = 779
			transfer_state = 778
			transfer_state = 781
			transfer_state = 776
			transfer_state = 777
			VIC = { country_event = LaR_france_vichy_notification_events.10 }
            GER = { country_event = LaR_france_vichy_notification_events.10 }
            ITA = { country_event = LaR_france_vichy_notification_events.10 }
		}
	}

	FRA_take_madagascar = {
		visible = {
			has_completed_focus = FRA_intervention_in_madagascar
		}
		fire_only_once = yes
		cost = 50
		days_remove = 70
		remove_effect = {
			transfer_state = 543
			transfer_state = 708
			transfer_state = 706
			transfer_state = 713
			VIC = { country_event = LaR_france_vichy_notification_events.12 }
            GER = { country_event = LaR_france_vichy_notification_events.12 }
            ITA = { country_event = LaR_france_vichy_notification_events.12 }
		}
	}

	FRA_take_syria = {
		visible = {
			has_completed_focus = FRA_intervention_in_syria
		}
		fire_only_once = yes
		cost = 50
		days_remove = 70
		remove_effect = {
			transfer_state = 553
			transfer_state = 554
			transfer_state = 680
			transfer_state = 677
			transfer_state = 799
			VIC = { country_event = LaR_france_vichy_notification_events.7 }
            GER = { country_event = LaR_france_vichy_notification_events.7 }
            ITA = { country_event = LaR_france_vichy_notification_events.7 }
		}
	}

}